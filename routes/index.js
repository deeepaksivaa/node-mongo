const express = require("express");
const router = express.Router();

const { Course } = require("../models/course");

// get all Course Details

router.get("/api/course", (req, res) => {
    Course.find({}, (err, data) => {
        if (!err) {
            res.send(data);
        } else {
            console.log(err);
        }
    });
});

// get specific Course Detail by ID

router.get("/api/course/:id", (req, res) => {
    Course.findById(req.params.id, (err, data) => {
        if (!err) {
            res.send(data);
        } else {
            console.log(err);
        }
    });
});

// Add new Course to collection

router.post("/api/course/add", (req, res) => {
    const courseData = new Course({
        name: req.body.name,
        duration: req.body.duration,
        prize: req.body.prize,
    });
    courseData.save((err, data) => {
        if (!err) {
            res.status(200).json({
                code: 200,
                message: "Course Added Successfully",
                addCourse: data,
            });
        } else {
            console.log(err);
        }
    });
});

// update specific Course by ID

router.put("/api/course/update/:id", (req, res) => {
    const courseData = {
        name: req.body.name,
        duration: req.body.duration,
        prize: req.body.prize,
    };
    Course.findByIdAndUpdate(
        req.params.id,
        { $set: courseData },
        { new: true },
        (err, data) => {
            if (!err) {
                res.status(200).json({
                    code: 200,
                    message: "Course Updated Successfully",
                    updateCourse: data,
                });
            } else {
                console.log(err);
            }
        }
    );
});

// Delete specific Course by ID

router.delete("/api/course/:id", (req, res) => {
    Course.findByIdAndRemove(req.params.id, (err, data) => {
        if (!err) {
            // res.send(data);
            res.status(200).json({
                code: 200,
                message: "Course deleted",
                deleteCourse: data,
            });
        } else {
            console.log(err);
        }
    });
});

module.exports = router;
