let mongoose = require("mongoose");

// Course Schema
const Course = mongoose.model("Course", {
    name: {
        type: String,
        required: true,
    },
    duration: {
        type: String,
        required: true,
    },
    prize: {
        type: String,
        required: true,
    },
});

module.exports = { Course };
